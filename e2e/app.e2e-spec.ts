import { ScarlariaWebPage } from './app.po';

describe('scarlaria-web App', function() {
  let page: ScarlariaWebPage;

  beforeEach(() => {
    page = new ScarlariaWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
